/**
 * Created by madajczak@gmail.com on 04.09.2018.
 */

var TableData = function (selector) {
    this.table = document.querySelector(selector);
    var headCells = ['Lp.', 'Nazwa', 'Cena', 'Ilość', 'Narzędzia'];
    var head = this.tableRow(headCells, true);
    this.table.appendChild(head);
    var initData = [
        [1, 'Procesor Intel', 100, 10],
        [2, 'Obudowa ATX', 20, 5],
        [3, 'Wiatraczek cichy', 80, 20]
    ];
    initData.forEach(function (dataRow) {
        this.addRow(dataRow);
    }.bind(this));
};
/**
 *
 * @param cells as Array of contents e.g. ['data', 'data']
 * @param th Boolean is th element
 * @returns {Element}
 */
TableData.prototype.tableRow = function (cells, th) {
    var tr = document.createElement('tr');
    cells.forEach(function (cellContent) {
        var elementType = th ? 'th' : 'td';
        var td = document.createElement(elementType);
        td.innerText = cellContent;
        tr.appendChild(td);
    });
    return tr;
};

TableData.prototype.toolCell = function (idx) {
    var td = document.createElement('td');
    var bttn = document.createElement('button');
    bttn.innerText = 'Usuń';
    bttn.dataset.index = idx;
    bttn.addEventListener('click', function (event) {
        var idx = parseInt(event.target.dataset.index);
        this.table.deleteRow(idx + 1);
    }.bind(this));
    td.appendChild(bttn);
    return td;
};

TableData.prototype.addRow = function (tableRowData) {
    var data = tableRowData;
    var rows = this.table.querySelectorAll('tr');
    data[0] = rows.length;
    var rw = this.tableRow(data);
    rw.appendChild(this.toolCell(rows.length - 1));
    this.table.appendChild(rw);
};

(function () {
    var tableData = new TableData('#table-data');
    var addRowBttn = document.querySelector('#add-article');

    var name = document.querySelector('#product-name');
    var number = document.querySelector('#number');
    var price = document.querySelector('#price');

    var getData = function () {
        return [
            '',
            name.value,
            price.value,
            number.value
        ];
    };


    var addBttnClickHandler = function () {
        tableData.addRow(getData());
    };

    addRowBttn.addEventListener('click', addBttnClickHandler);
}());